#!/bin/bash
set -e

# if thrown flags immediately,
# assume they want to run the blockchain daemon
if [ "${1:0:1}" = '-' ]; then
	set -- monerod "$@"
fi

# otherwise, don't get in their way
exec "$@"
